def COCKROACH_IP=[]
def COCKROACH_NETWORK
def COCKROACH_CLUSTER
def COCKROACH_TYPE
def COCKROACH_PORT_UI
def COCKROACH_PORT_STRING
def COCKROACH_PORT
pipeline {
    agent any 
    parameters {
        string(name: 'PROJECTO', defaultValue: 'my-own-project-252421', description: 'Nombre Projecto Cloud')
        string(name: 'CLUSTER_NAME', defaultValue: 'Test Cluster', description: 'Nombre del Cluster Cockroach')
        choice(name: 'ENV', choices: ['prod', 'dev'], description: 'Ambiente')
        string(name: 'NODOS', defaultValue: '1', description: 'Cantidad de Nodos Cluster')
    }
    stages {
        stage ('Creación Máquinas') {
            steps {

            // SET PROJECTO
            sh "gcloud config set project ${params.PROJECTO}" 
                
                script {

                // SET TIPO DE MAQUINA
                if ("${params.ENV}" == 'prod') {COCKROACH_TYPE='n1-highcpu-4' } else {COCKROACH_TYPE='n1-standard-2'} 

                // CREACION DE MAQUINAS
                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++) {
                sh "gcloud beta compute --project=${params.PROJECTO} instances create cockroach-${params.ENV}-${loopIndex} --zone=us-central1-a --machine-type=${COCKROACH_TYPE} --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=812385867631-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server,https-server,cockroach-cluster-db-tier --image=debian-9-stretch-v20200420 --image-project=debian-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=cockroach-${params.ENV}-${loopIndex} --create-disk=mode=rw,size=100,type=projects/${params.PROJECTO}/zones/us-central1-a/diskTypes/pd-ssd,name=cockroach-${params.ENV}-disk-${loopIndex},device-name=cockroach-${params.ENV}-disk-${loopIndex} --reservation-affinity=any" 
                }

                // REGLAS DE FIREWALL
                sh """
                gcloud compute --project=${params.PROJECTO} firewall-rules create cockroach-cluster-db-tcp-26257 --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:26257 --source-tags=cockroach-cluster-db-tier --target-tags=cockroach-cluster-db-tier

                gcloud compute --project=${params.PROJECTO} firewall-rules create cockroach-cluster-db-tcp-8080 --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:8080 --source-tags=cockroach-cluster-db-tier --target-tags=cockroach-cluster-db-tier

                gcloud compute --project=${params.PROJECTO} instance-groups unmanaged create cockroach-cluster --zone=us-central1-a
                """
                
                // ADICIONAR AL GRUPO DE INSTANCIAS Y CAPTURA DE IP INTERNA

                COCKROACH_PORT=26257

                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++) {

                sh "gcloud compute --project=${params.PROJECTO} instance-groups unmanaged add-instances cockroach-cluster --zone=us-central1-a --instances=cockroach-${params.ENV}-${loopIndex}"

                COCKROACH_NETWORK= sh(script: "gcloud compute instances describe cockroach-${params.ENV}-${loopIndex} --zone=us-central1-a --format='value(networkInterfaces.networkIP)'", returnStdout: true)

                echo COCKROACH_PORT

                echo COCKROACH_NETWORK

                COCKROACH_PORT_STRING=COCKROACH_PORT.toString()

                COCKROACH_NETWORK=COCKROACH_NETWORK.trim().concat(':').concat(COCKROACH_PORT_STRING)

                COCKROACH_IP.add(COCKROACH_NETWORK)

                echo COCKROACH_IP

                COCKROACH_PORT++
                }
                       }   
            } 
        }
        stage('Instalacion Cassandra') {
            steps {
                script {
                
                // PROVISIONAMIENTO E INSTALACION DE COCKROACH
                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++){
                sh """ 
                gcloud compute ssh cockroach-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'sudo apt-get install -y apt-transport-https'
                gcloud compute ssh cockroach-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'sudo apt-get update'
                gcloud compute ssh cockroach-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.6.linux-amd64.tgz | tar  xvz'
                gcloud compute ssh cockroach-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'sudo cp -i cockroach-v19.2.6.linux-amd64/cockroach /usr/local/bin/'
                """
                }
                       }
            }  
        }
        stage('Creacion de Cluster') {
            steps {
                script {
                //CREACION CLUSTER COCKROACH
                
                COCKROACH_PORT_UI=8080
                COCKROACH_PORT=26257

                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++){
                sh """
                COCKROACH_NETWORK=\$(gcloud compute instances describe cockroach-${params.ENV}-${loopIndex} --zone=us-central1-a --format='value(networkInterfaces.networkIP)')

                gcloud compute ssh cockroach-${params.ENV}-${loopIndex} --zone=us-central1-a --command "cockroach start --insecure --listen-addr=\$COCKROACH_NETWORK:"${COCKROACH_PORT}"  --join="${COCKROACH_CLUSTER}" --http-addr=\$COCKROACH_NETWORK:"${COCKROACH_PORT_UI}" --store=cockroach-data-${loopIndex} --background"
                """
                COCKROACH_PORT++
                COCKROACH_PORT_UI++
                }
            }
            }
        }  
        stage('Inicio de Cluster & Validación') {
            steps {
                 script {
                // INICIO CLUSTER
                sh """
                COCKROACH_NETWORK=\$(gcloud compute instances describe cockroach-${params.ENV}-0 --zone=us-central1-a --format='value(networkInterfaces.networkIP)')

                cockroach init --host \$COCKROACH_NETWORK:26257 --insecure"
                """
                //VALIDACIÓN FUNCIONAMIENTO
                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++){
                sh """ 
                cockroach node status
                cockroach sql --insecure -e 'SHOW databases;'"
                """
                }
                }  
            }
        }    
    }
}
